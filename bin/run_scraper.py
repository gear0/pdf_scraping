#!/usr/bin/env python3
import argparse
import glob
import multiprocessing as mp
import subprocess
import threading as th
from os.path import exists, join
from pathlib import Path

from pdf_scraping.config import Config
from pdf_scraping.pdf_converter import PDFconverter
from pdf_scraping.pdf_downloader import PDFdownloader
from pdf_scraping.pdf_scraper import PDFscraper


def argparser():
    parser = argparse.ArgumentParser(description='Starts the pdf scraper')
    optional = parser._action_groups.pop()
    # required_named = parser.add_argument_group('required arguments')
    # required_named.add_argument('--config',
    #                             dest='config',
    #                             action='store',
    #                             required=True,
    #                             help='absolute path to config file')
    optional.add_argument(
        '--number-of-papers',
        dest='amount',
        action='store',
        default=-1,
        type=int,
        help='number of papers to download (default: -1 -> means all)')
    optional.add_argument(
        '--headless',
        action='store_true',
        dest='headless',
        default=False,
        help='flag for starting Chrome headless (default: False)')
    optional.add_argument(
        '--threads',
        action='store',
        type=int,
        dest='threads',
        default=1,
        help=
        'number of threads to be used for downloading the papers (default: 1)')

    parser._action_groups.append(optional)
    args = parser.parse_args()
    return args


def chunks(l: list, n: int):
    """Yield successive n-sized chunks from l"""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def run_parallel_threads(worker_functions: list, threads: int,
                         is_download: bool):
    """runs functions in worker_functions in parallel threads"""
    for chunk in chunks(worker_functions, threads):
        if is_download:
            threads = [
                th.Thread(target=function, args=(args.amount, ))
                for function in chunk
            ]
        else:
            threads = [th.Thread(target=function) for function in chunk]
        i = 0
        for t in threads:
            i += 1
            print(f"Process {i} started")
            t.start()

        for t in threads:
            t.join()


def run_parallel_processes(worker_functions: list, threads: int,
                           is_download: bool):
    """runs functions in worker_functions in parallel processes"""
    for chunk in chunks(worker_functions, threads):
        if is_download:
            processes = [
                mp.Process(target=function, args=(args.amount, ))
                for function in chunk
            ]
        else:
            processes = [mp.Process(target=function) for function in chunk]
        i = 0
        for p in processes:
            i += 1
            print(f"Process {i} started")
            p.start()

        for p in processes:
            p.join()


if __name__ == '__main__':
    args = argparser()
    areas = Config.EPAPER_AREAS.keys()
    downloaders = []
    converters = []
    scrapers = []

    for area in areas:
        downloaders.append(
            PDFdownloader(headless=args.headless,
                          area=area).collect_and_download)

    # run_parallel_processes(downloaders, args.threads, True)
    run_parallel_processes(downloaders, args.threads, True)

    for area in areas:
        converters.append(PDFconverter(area).convert_pdf)

    # run_parallel_processes(converters, args.threads, False)
    run_parallel_processes(converters, args.threads, False)

    for area in areas:
        scrapers.append(PDFscraper(area).articles_to_json)

    # run_parallel_processes(scrapers, args.threads, False)
    run_parallel_processes(scrapers, args.threads, False)
