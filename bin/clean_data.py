#!/usr/bin/env python3
import argparse
from pdf_scraping.cleaner import Cleaner


def argparser():
    parser = argparse.ArgumentParser(
        description='Cleans data produced by the scraper')
    optional = parser._action_groups.pop()
    optional.add_argument(
        '--clean',
        nargs='+',
        action='store',
        dest='to_clean',
        help='''specify which files should be deleted after processing is done.
        You can specify "pdf", "xml", "json", "logs"''')
    parser._action_groups.append(optional)
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = argparser()
    # remove files after processing
    if len(args.to_clean) > 0:
        with_pdf = False
        with_xml = False
        with_json = False
        with_logs = False
        cleaner = Cleaner()
        if 'pdf' in args.to_clean:
            with_pdf = True
        if 'xml' in args.to_clean:
            with_xml = True
        if 'json' in args.to_clean:
            with_json = True
        if 'logs' in args.to_clean:
            with_logs = True
        cleaner.clean_local_data(with_pdf, with_xml, with_json, with_logs)
