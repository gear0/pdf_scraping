from os import remove, listdir
from os.path import join, isfile
from .config import Config


class Cleaner(object):
    def __init__(self):
        self.areas = Config.EPAPER_AREAS.keys()

    def clean_local_data(self, with_pdf: bool, with_xml: bool, with_json: bool,
                         with_logs: bool):
        if with_pdf:
            for area in self.areas:
                subfolder = join(Config.PDF_STORAGE_PATH, area)
                self._delete_all(subfolder)

        if with_xml:
            for area in self.areas:
                subfolder = join(Config.XML_STORAGE_PATH, area)
                self._delete_all(subfolder)

        if with_json:
            for area in self.areas:
                subfolder = join(Config.JSON_STORAGE_PATH, area)
                self._delete_all(subfolder)
        if with_logs:
            self._delete_all(Config.LOGGING_DIR)

    def _delete_all(self, path: str):
        files = [f for f in listdir(path) if isfile(join(path, f))]
        for f in files:
            remove(join(path, f))
