import glob
import subprocess
from os import makedirs
from os.path import join, exists, isdir
from pathlib import Path
from .config import Config
from .logger.logger import (for_all_methods, logging_decorator)

@for_all_methods(logging_decorator, __name__)
class PDFconverter(object):

    def __init__(self, area):
        self.area = area

    def convert_pdf(self):
        """converts pdfs to xml"""
        if isdir(join(Config.PDF_STORAGE_PATH, self.area)):
            pdfs = glob.glob(join(Config.PDF_STORAGE_PATH, self.area, '*.pdf'))

            if not isdir(join(Config.XML_STORAGE_PATH, self.area)):
                makedirs(join(Config.XML_STORAGE_PATH, self.area))

            for pdf_path in pdfs:
                _,_,pdf = pdf_path.rpartition("/")
                name = pdf.replace(".pdf", "")

                xml = name + ".xml"
                xml_path = join(Config.XML_STORAGE_PATH, self.area, xml)
                xml_file = Path(xml_path)

                if not xml_file.exists():
                    p = subprocess.Popen(["pdf2txt.py", pdf_path, "-o", xml_path])
                    p.wait()
