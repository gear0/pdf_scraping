from os import makedirs
from os.path import abspath, join, dirname, isdir
from pathlib import Path


class Config():
    """Class for accessing the configuration conviniently"""

    CHROMEDRIVER_PATH = join(str(Path.home()),
                             'pdf_scraping_data/chromedriver')
    CHROMEDRIVER_BINARY = join(CHROMEDRIVER_PATH, 'chromedriver')
    EPAPER_ARCHIVE_BASE_URL = 'https://www.wzo.de/archivausgaben/'
    EPAPER_DOWNLOAD_BASE_URL = 'https://www.calameo.com/download/'
    EPAPER_AREAS = {
        'muellheim': 'ReblandKurier-Muellheim',
        'rheinfelden': 'Wochenblatt-Rheinfelden',
        'markgraeflerland': 'Wochenblatt-Markgraeflerland',
        'weil': 'Wochenblatt-Weil',
        'loerrach': 'Wochenblatt-Loerrach',
        'schopfheim': 'Wochenblatt-Schopfheim',
        'wiesental': 'Wochenblatt-oberes-Wiesental'
    }

    LOGGING_ENABLED = True
    LOGGING_DIR = join(str(Path.home()), 'pdf_scraping_data/log')
    LOGLEVEL = 'DEBUG'

    PDF_STORAGE_PATH = join(str(Path.home()), 'pdf_scraping_data/pdf')
    XML_STORAGE_PATH = join(str(Path.home()), 'pdf_scraping_data/xml')
    JSON_STORAGE_PATH = join(str(Path.home()), 'pdf_scraping_data/json')


# make the directories specified in this file
try:
    if not isdir(Config.PDF_STORAGE_PATH):
        makedirs(Config.PDF_STORAGE_PATH)

    if not isdir(Config.CHROMEDRIVER_PATH):
        makedirs(Config.CHROMEDRIVER_PATH)

    if not isdir(Config.XML_STORAGE_PATH):
        makedirs(Config.XML_STORAGE_PATH)

    if not isdir(Config.JSON_STORAGE_PATH):
        makedirs(Config.JSON_STORAGE_PATH)

    if not isdir(Config.LOGGING_DIR):
        makedirs(Config.LOGGING_DIR)
except OSError:
    pass
