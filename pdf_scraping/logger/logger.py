from os import makedirs
from os.path import join, exists
from pathlib import Path
from functools import wraps
from pdf_scraping.config import Config
# import logging
from loguru import logger


LOG_DIR = Config.LOGGING_DIR
if str(Config.LOGLEVEL).lower() == 'info':
    LEVEL = 'INFO'
elif str(Config.LOGLEVEL).lower() == 'debug':
    LEVEL = 'DEBUG'
elif str(Config.LOGLEVEL).lower() == 'warning':
    LEVEL = 'WARNING'
elif str(Config.LOGLEVEL).lower() == 'error':
    LEVEL = 'ERROR'
elif str(Config.LOGLEVEL).lower() == 'critical':
    LEVEL = 'CRITICAL'

FORMAT = '{time:YYYY-MM-DD at HH:mm:ss} | {level: <4} | {message}'
MAX_LOG_SIZE = '5 MB'  # MB

def for_all_methods(decorator, module):
    """
    Decorator for classes which decorates every
    function of the class with the logger decorator
    """
    @wraps(decorator)
    def decorate(cls):
        for attr in cls.__dict__:
            if callable(getattr(cls, attr)) and attr is not '__init__':
                setattr(cls, attr, decorator(getattr(cls, attr), module))
        return cls

    return decorate

def make_filter(name):
    """function to filter logs per logged module"""
    def filter(record):
        return record['extra'].get('name') == name
    return filter

def logging_decorator(funct, module):
    """
    Decorater function for logging
    """
    if Config.LOGGING_ENABLED:
        # remove default handler to prevent logging to stdout
        if 0 in logger._handlers.keys():
            logger.remove(0)
        # add new handler for the given module
        logger.add('{}/{}.log'.format(Config.LOGGING_DIR, module), level=LEVEL,
                      format=FORMAT,
                   rotation=MAX_LOG_SIZE, enqueue=True, catch=True, retention=10, filter=make_filter(module))

        # bind logger to name so that filtering per module works
        cust_logger = logger.bind(name=module)

        # run this code before running funct
        @wraps(funct)
        def wrapper(*args, **kwargs):
            try:
                cust_logger.info('Running {} with arguments: {} , {}'.format(
                    funct.__name__, args, kwargs))
            except:
                cust_logger.info('Some information could not be logged')
            return funct(*args, **kwargs)

        return wrapper
    else:
        return funct
