import bs4
import re
import json
import glob
from os import makedirs
from os.path import (abspath, join, dirname, exists, isdir)
from pathlib import Path


from os import makedirs
from .config import Config
from .logger.logger import (for_all_methods, logging_decorator)

# currently there is no logging of this class because of very high logging volume
# and logging errors (probably race conditions) while handling log rotation
# in multiprocess context
# @for_all_methods(logging_decorator, __name__)
class PDFscraper(object):
    def __init__(self, area):
        self.area = area

    def read_file(self, path) -> str:
        """reads content of given file"""
        try:
            f = open(path, "r")
            content = f.read()
            f.close()
            return content
        except OSError:
            pass

    def get_font(self, font) -> str:
        """gets font of xml element"""
        pattern1 = re.compile(r'SpiegelCd-Bold')
        pattern2 = re.compile(r'ZineSerifDis-RegularRoman')
        pattern3 = re.compile(r'SpiegelCd')
        if pattern1.search(font):
            return "SpiegelCd-Bold"
        if pattern2.search(font):
            return "ZineSerifDis-RegularRoman"
        if pattern3.search(font):
            return "SpiegelCd"
        else:
            return ""

    def save_json(self, data, path, file_name):
        """writes data to file"""
        if not exists(path):
            makedirs(path)

        # json_name = self.xml_name.replace(".xml", ".json")
        json_file = join(path, file_name)

        with open(json_file, 'w') as f:
            f.write(data)

    def articles_to_json(self):
        """gets content of articles"""
        forbidden_headlines = [
            "• • • • • ",
            "KONTAKT\n",
            "LÖRRACH • INZLINGEN\n",
            "BAD BELLINGEN • EFRINGEN-KIRCHEN • KANDERN • SCHLIENGEN\n",
            "MÜLLHEIM • NEUENBURG • HEITERSHEIM • MARKGRÄFLERLAND\n",
            "RHEINFELDEN • GRENZACH-WYHLEN • SCHWÖRSTADT\n",
            "SCHOPFHEIM • HAUSEN • MAULBURG • STEINEN\n",
            "WEIL AM RHEIN • BINZEN • EIMELDINGEN\n",
            "SCHÖNAU • TODTNAU • ZELL • KLEINES WIESENTAL\n",
            "Ärzte\n",
            "Zahnärzte\n",
            "Apotheken\n",
            "Telefonseelsorge\n",
            "MONTAG\n",
            "DIENSTAG\n",
            "MITTWOCH\n",
            "DONNERSTAG\n",
            "FREITAG\n",
            "SAMSTAG\n",
            "SONNTAG\n",
            "Biowetter\n",
            "IMMOBILIEN-SPIEGEL\n",
            "WOHNUNGSMARKT\n",
            "FAHRZEUGMARKT\n",
            "VERSCHIEDENES\n",
            "KAUFGESUCHE\n",
            "URLAUB / FREIZEIT\n",
            "FLOHMARKT\n",
            "BEKANNTSCHAFTEN\n",
            "TIERE\n",
            "DIE GUTE TAT\n",
            "KONTAKTE\n",
            "IMPRESSUM\n",
            "Fettzeile\n"
            ]
        # for area in areas:
        if isdir(join(Config.XML_STORAGE_PATH, self.area)):
            xmls = glob.glob(join(Config.XML_STORAGE_PATH, self.area, '*.xml'))

            if not isdir(join(Config.JSON_STORAGE_PATH, self.area)):
                makedirs(join(Config.JSON_STORAGE_PATH, self.area))

            for i in range(len(xmls)):
                xml_path = xmls[i]
                xml = xmls[i].replace(Config.XML_STORAGE_PATH + "/" + self.area + "/", "")
                name = xml.replace(".xml", "")
                _, _, epaper_id = name.rpartition('_')
                date, _, _ = name.rpartition('_' + self.area + '_')

                json = name + ".json"
                json_path = join(Config.JSON_STORAGE_PATH, self.area)
                json_file = Path(join(json_path, json))

                if json_file.exists():
                    continue

                content = self.read_file(xml_path)
                soup = bs4.BeautifulSoup(content, "xml")
                all_textboxes = soup.find_all('textbox')

                main_content = {}
                index = 0
                main_content["articles"] = {}
                last_headline = ""
                allowed_fonts = ["SpiegelCd-Bold", "ZineSerifDis-RegularRoman", "SpiegelCd"]
                article_locked = False #nach Article-Font darf kein Subheadline-Font mehr kommen

                #Textboxen scannen
                for textbox in all_textboxes:
                    headline_content, article_content, subheadline_content = "", "", ""
                    scope_headline = False #werden benötigt um Leerzeichen zuordnen zu können
                    scope_subheadline = False
                    scope_article = False
                    is_advert = False

                    

                    all_texts = textbox.find_all('text')
                    if not all_texts:
                        continue
                    first_font = self.get_font(all_texts[0]['font'])
                    if not first_font in allowed_fonts:
                        continue

                    for text in all_texts:
                        if text.has_attr('font'):
                            font = self.get_font(text['font'])
                            if not self.get_font(font) in allowed_fonts:
                                is_advert = True
                                break
                            if font == "SpiegelCd-Bold":
                                if float(text['size']) > 7:
                                    headline_content += text.get_text()
                                    scope_headline = True
                                    scope_subheadline = False
                                    scope_article = False
                                    article_locked = False
                                    continue
                                if float(text['size']) >= 6.6:
                                    article_content += text.get_text()
                                    scope_headline = False
                                    scope_subheadline = False
                                    scope_article = True
                                    continue
                                else:
                                    continue

                            if font == "SpiegelCd":
                                if not float(text['size']) > 6.2:
                                    continue
                                subheadline_content += text.get_text()
                                scope_headline = False
                                scope_subheadline = True
                                scope_article = False
                                continue


                            if font == "ZineSerifDis-RegularRoman":
                                if 6 > float(text['size']) > 8:
                                    is_advert = True
                                    break
                                article_content += text.get_text()
                                scope_headline = False
                                scope_subheadline = False
                                scope_article = True
                                article_locked = True


                        else: #Leerzeichen und Absätze
                            if scope_headline:
                                headline_content += text.get_text()
                                continue
                            if scope_subheadline:
                                subheadline_content += text.get_text()
                                continue
                            if scope_article:
                                article_content += text.get_text()


                    if is_advert:
                        continue

                    if headline_content and not headline_content in forbidden_headlines:
                        index += 1

                        main_content["articles"][index] = {
                            "headline": "",
                            "subheadline": "",
                            "article": "",
                            "url": "".join([Config.EPAPER_DOWNLOAD_BASE_URL, epaper_id]),
                            "date": "" + date
                        }

                        headline_content = headline_content.replace("-\n", "")
                        headline_content = headline_content.replace("\n", " ")
                        pattern = re.compile(r'[0-9]+°')
                        if re.match(pattern, headline_content):
                            continue
                        last_headline = headline_content
                        main_content["articles"][index]["headline"] = last_headline
                        # main_content["articles"][index]["subheadline"] = ""
                        # main_content["articles"][index]["article"] = ""
                        # {
                        #     "headline": last_headline,
                        #     "subheadline": "",
                        #     "article": ""
                        # }

                    if not article_locked:
                        if not index in main_content["articles"]:
                            main_content["articles"][index] = {
                                "headline": "",
                                "subheadline": "",
                                "article": "",
                                "url": "".join([Config.EPAPER_DOWNLOAD_BASE_URL, epaper_id]),
                                "date": "" + date
                            }
                        subheadline_content = subheadline_content.replace("-\n", "")
                        subheadline_content = subheadline_content.replace("\n", " ")
                        main_content["articles"][index]["subheadline"] += subheadline_content
                    article_content = article_content.replace("-\n", "")
                    article_content = article_content.replace("\n", " ")
                    main_content["articles"][index]["article"] += article_content

                self.save_json(str(main_content), json_path, json)
