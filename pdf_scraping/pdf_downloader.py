from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import (Select, WebDriverWait)
from selenium.webdriver.support import expected_conditions as EC
from os.path import (abspath, join, dirname, exists, isfile)
from os import makedirs
from .logger.logger import (for_all_methods, logging_decorator)
from .config import Config
from requests import (get, HTTPError)


@for_all_methods(logging_decorator, __name__)
class PDFdownloader(object):
    def __init__(self, headless: bool, area: str):
        self.BASE_URL = Config.EPAPER_ARCHIVE_BASE_URL
        self.WEBDRIVER_PATH = Config.CHROMEDRIVER_BINARY
        self.area = area
        self.areas = Config.EPAPER_AREAS
        self.link_collection = {}
        self.options = Options()
        self.options.headless = headless
        self.browser = webdriver.Chrome(executable_path=self.WEBDRIVER_PATH,
                                        options=self.options)
        self.pagination_links = self._get_pagination_links()
        self.link_collection[self.area] = []
        self.links_and_dates = {}

    def collect_and_download(self, num_papers: int):
        """
        Collect downloadlinks and download the corresponding
        PDFs in one step

        Args:
        num_papers: number of papers per edition to download
        """
        try:
            self.collect_n_links(num_papers)
            self.download_papers()
        finally:
            self.browser.quit()

    def collect_n_links(self, num_papers: int) -> dict:
        """
        recursivly collect the specified number of
        links for downloading the papers
        Args:
        num_papers: number of papers per edition to download
        """
        # if no number of papers is specified, num_papers is
        # set to -1 and all downloadlinks for an edition are
        # collected
        if num_papers == -1:
            return self._collect_all_links()

        # get download buttons
        dl_buttons_per_page = WebDriverWait(self.browser, 30).until(
            EC.presence_of_all_elements_located(
                (By.CLASS_NAME, 'btn-warning')))

        if len(dl_buttons_per_page) > num_papers:
            dates = self._get_dates(num_papers)
            self.link_collection[self.area] += [
                link.get_attribute('href') for link in dl_buttons_per_page
            ][:num_papers]

            # update links_and_dates dictionary
            self.links_and_dates.update(
                dict(zip(dates, self.link_collection[self.area])))
            self.pagination_links.pop()
        else:
            # if there are more links to collect than download buttons
            # on a page then collect all links from this page and
            # move on to the next page
            remaining_papers = num_papers - len(dl_buttons_per_page)
            self.link_collection[self.area] += [
                link.get_attribute('href') for link in dl_buttons_per_page
            ]

            self.links_and_dates.update(
                dict(zip(dates, self.link_collection[self.area])))

            self.browser.get(self.pagination_links.pop())
            # run function again
            self.collect_n_links(remaining_papers)

    def _collect_all_links(self):
        """
        Shortcut for collecting all links of a edition
        """
        # get pagination elements
        pagination_links = self._get_pagination_links()
        # do until no page is left
        for link in pagination_links:
            self.browser.get(link)
            dl_buttons = WebDriverWait(self.browser, 30).until(
                EC.presence_of_all_elements_located(
                    (By.CLASS_NAME, 'btn-warning')))

            dates = self._get_dates(num_papers=len(dl_buttons))
            self.link_collection[self.area] += [
                link.get_attribute('href') for link in dl_buttons
            ]
            self.links_and_dates.update(
                dict(zip(dates, self.link_collection[self.area])))

    def _get_dates(self, num_papers: int) -> list:
        """
        get dates of papers on one page
        """
        dates = []
        name_and_dates = WebDriverWait(self.browser, 30).until(
            EC.presence_of_all_elements_located(
                (By.XPATH, '//tr/th[@colspan=2]')))
        for name_and_date in name_and_dates[:num_papers]:
            _, _, date = name_and_date.text.rpartition('|')
            day, month, year = date.strip().split('.')
            date = '_'.join([year, month, day])
            dates.append(date)
        return dates

    def _select_edition(self):
        """
        select the edition to collect links for
        """
        select_elem = Select(
            WebDriverWait(self.browser, 30).until(
                EC.presence_of_element_located((By.ID, 'gebiet'))))
        option_dict = {
            e.get_attribute('value'): e
            for e in select_elem.options
        }

        # select area
        option_dict[self.areas[self.area]].click()

    def _get_pagination_links(self) -> list:
        """
        get all pagination links for one edition
        """
        # do once per edition
        self.browser.get(self.BASE_URL)
        # get the page for the specified area
        self._select_edition()
        pagination_elements = self.browser.find_elements_by_xpath(
            '//*[contains(@class, "pagination")]/li/a')

        return [e.get_attribute('href') for e in pagination_elements]

    def download_papers(self):
        """
        This function is for downloading epaper pdf files.
        It saves the files to the directory specified in
        config.py
        """
        download_links = {}
        # build download urls
        for date, link in self.links_and_dates.items():
            _, sep, epaper_id = link.rpartition('/')
            download_links[date] = ''.join(
                [Config.EPAPER_DOWNLOAD_BASE_URL, epaper_id])

        # fake user agent, otherwise no download is possible
        headers = {
            'User-Agent':
            'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36'
        }
        # download pdf if not already downloaded
        for date, link in download_links.items():
            _, sep, epaper_id = link.rpartition('/')
            fname = '.'.join(['_'.join([date, self.area, epaper_id]), 'pdf'])
            media_dir = join(Config.PDF_STORAGE_PATH, self.area)
            if not exists(media_dir):
                makedirs(media_dir)
            stor_loc = join(media_dir, fname)
            if not isfile(stor_loc):
                # allow_redirects because files are actually hosted
                # on amazon S3 Bucket
                with get(link,
                         stream=True,
                         headers=headers,
                         allow_redirects=True) as r:
                    if r.status_code == 200:
                        with open(stor_loc, 'wb') as f:
                            for chunk in r.iter_content(chunk_size=1024):
                                f.write(chunk)
                    else:
                        raise HTTPError
