from setuptools import setup

with open('./requirements.txt', 'r') as f:
    requirements = f.readlines()

setup(name='pdf_scraping',
      version='1.0',
      description='Tool for extracting text from pdf newspaper',
      url='https://gitlab.com/gear0/pdf_scraping',
      author='two guys',
      author_email='',
      license='LICENSE',
      packages=['pdf_scraping', 'pdf_scraping.logger'],
      zip_safe=False,
      scripts=['bin/run_scraper.py', 'bin/clean_data.py'],
      install_requires=requirements)
